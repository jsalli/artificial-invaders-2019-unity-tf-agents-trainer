import json

lista = None

with open('ArtificialInvaderLearning.json', 'r') as file:
    json_string = file.read()
    lista = json.loads(json_string)

current_level = 0
reset_params = {}
for key in lista["parameters"].keys():
    print("key: {}, value: {}".format(key, lista["parameters"][key][current_level]))
    reset_params[key] = lista["parameters"][key][current_level]

print("####")

print(reset_params)