import os

import numpy as np
from tf_agents.environments import py_environment, utils
from tf_agents.specs import array_spec
from tf_agents.trajectories import time_step
from ai_multi_game import AIMultiGame
import json
import random

class AIMultiEnvironment(py_environment.PyEnvironment):
    def __init__(self, worker_id, shared_training_status):
        super().__init__()
        self._shared_training_status = shared_training_status
        worker_id = worker_id if worker_id is not None else random.randint(0,10000)
        game, batch_size = self.configure_ai(worker_id)
        self._game = game
        self._batch_size = batch_size
        self._num_actions = self._game.get_num_of_actions()
        self._num_observations = self._game.get_num_of_observations()
        self._level = 0

        self._action_spec = array_spec.BoundedArraySpec(shape=(), dtype=np.int32, minimum=0, maximum=self._num_actions - 1, name='action')
        self._observation_spec = array_spec.BoundedArraySpec(shape=(self._num_observations,), dtype=np.float64, minimum=0, maximum=1, name='observation')
        
        with open('ArtificialInvaderLearning.json', 'r') as file:
            json_string = file.read()
            self._unity_scene_reset_parameters = json.loads(json_string)
        
        self._current_level = -1
        self._max_level = len(self._unity_scene_reset_parameters["thresholds"])


    # @staticmethod
    def configure_ai(self, worker_id):
        game = AIMultiGame(worker_id)
        batch_size = game.init_game()
        return game, batch_size

    @property
    def batched(self):
        return True


    def batch_size(self):
        return self._batch_size


    def action_spec(self):
        return self._action_spec


    def observation_spec(self):
        return self._observation_spec


    def _reset(self):
        reset_params = None
        if (
            self._unity_scene_reset_parameters is not None and
            self._unity_scene_reset_parameters["measure"] == "reward"):
            if (
                self._shared_training_status["AverageReturn"] > self._unity_scene_reset_parameters["thresholds"][self._current_level] or
                self._current_level == -1):
                if self._current_level < self._max_level:
                    self._current_level += 1
                    print("### Loading new level: {}".format(self._current_level + 1))
                    reset_params = {}
                    for key in self._unity_scene_reset_parameters["parameters"].keys():
                        reset_params[key] = self._unity_scene_reset_parameters["parameters"][key][self._current_level]
                    print("### Parameters are: {}".format(reset_params))
                    self._level += 1

        self._game.reset(reset_params)
        time_steps = [None] * self.batch_size()
        observations = self._game.get_observations()
        for i in range(len(time_steps)):
            time_steps[i] = time_step.restart(observations[i])
        return time_steps

    def _step(self, actions):
        time_steps = [None] * self.batch_size()
        previousEpisodes_finished =  self._game.are_episodes_finished()
        if len(time_steps) is not len(previousEpisodes_finished):
            raise ValueError(
                "time_steps and episodes_finished lists length is not the same"
                "time_steps length: {} vs. episodes_finished length {}".format(len(time_steps), len(previousEpisodes_finished)))
        

          # The last action ended the episode. Ignore the current action and start a new episode.
        #   return self.reset()
          # return a single arena "time_step.restart" for arenas which are finished
          # For other return "time_step.termination" or "time_step.transition"

        # execute action and receive reward
        # observations = self._game.get_observations()

        for i in range(len(time_steps)):
            if previousEpisodes_finished[i] is True:
                # time_steps[i] = time_step.restart(observations[i])
                actions[i] = 0 #Don't make an action in Unity

        rewards = self._game.make_action(np.array(actions, dtype=np.int32))
        observations = self._game.get_observations()
        newEpisodes_finished =  self._game.are_episodes_finished()
        for i in range(len(time_steps)):
            if previousEpisodes_finished[i] == True:
                time_steps[i] = time_step.restart(observations[i])
            elif newEpisodes_finished[i] == True:
                time_steps[i] = time_step.termination(observations[i], rewards[i])
            elif newEpisodes_finished[i] == False:
                time_steps[i] = time_step.transition(observations[i], rewards[i])
            else:
                raise ValueError(
                "We shouldn't be here. Something wrong with episodes finished/not finished")
        return time_steps

    def close(self):
        if self._game is not None:
            self._game.close()